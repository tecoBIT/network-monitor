<?php
//Cache Disabling Headers
    header('Cache-Control: no-cache, no-store, must-revalidate');              // HTTP 1.1 Cache Disabling
    header('Pragma: no-cache');                                                // HTTP 1.0  Cache Disabling
    header('Expires: 0');                                                      // Proxies Cache Disabling (Probably unnecessary)
    header('X-Robots-Tag: none');                                              // This tells google bot "noindex,nofollow"

//Minify the HTML Buffer before sending to the client
    function minifyHTML($Buffer) {
        $aSearch = array('/\>[^\S ]+/s',                                       //strip whitespaces after tags, except space
                         '/[^\S ]+\</s',                                       //strip whitespaces before tags, except space
                         '/(\s)+/s');                                          //shorten multiple whitespace sequences
        $aReplace = array( '>', '<', '\\1' );
        $Buffer = preg_replace($aSearch, $aReplace, $Buffer);
        return $Buffer;
    }
    if(SITE_COMPRESS == true) { ob_start("minifyHTML"); }                      //Toggled in global.config.php
?>
<html>
    <head>
        <title>Network Monitor</title>
        <meta name="robots" content="noindex,nofollow" />
        <meta name="viewport" content="width=device-width, initial-scale=0.75" />
        <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
        <meta content="utf-8" http-equiv="encoding">
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="expires" content="Sun, 15 Aug 1976 10:34:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
        <link rel="canonical" href="<?php echo SITE_HOME ?>" />
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
        <link href="includes/css/bootstrap.min.css" rel="stylesheet" />
        <link href="includes/css/style.min.css" rel="stylesheet" />
        <script src="includes/js/jquery.min.js" defer></script>
        <script src="includes/js/global.min.js" defer></script>
    </head>
    <body>
        <div id="container">
            Loading...
        </div>
    </body>
</html>