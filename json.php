<?php
//Cache Disabling Headers
    header('Cache-Control: no-cache, no-store, must-revalidate');               // HTTP 1.1 Cache Disabling
    header('Pragma: no-cache');                                                 // HTTP 1.0  Cache Disabling
    header('Expires: 0');                                                       // Proxies Cache Disabling (Probably unnecessary)

//Necessary Libraries
    include_once('application/configuration/global.config.php');
    include_once('application/classes/sqlite.class.php');

//Open Database Connection
    $oSQL = new SQLiteDatabase(DB_PATH);

//Query Back the Entirety of the tests + most recent value
    $sSQL = "SELECT T.ID AS ID, T.Title AS Title, T.Address AS Address, T.Port AS Port, " .
            "R.Status AS Status, R.Timestamp AS Time FROM Tests AS T " .
            "LEFT JOIN Results AS R ON T.ID = R.TestID " .
            "WHERE T.ID IS NOT NULL " .
            "GROUP BY T.ID " .
            "ORDER BY T.SortOrder ASC";
    $aResults = $oSQL->query($sSQL,array());
    unset($oSQL);

//Echo the JSON Results
     echo json_encode($aResults);
?>