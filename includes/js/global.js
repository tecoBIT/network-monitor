$(document).ready(function(){
//GLOBALS =====================================================================
    var sURL = '/json.php';                                                                                             //EDIT: Path to the JSON Data
    var sTemplateURL = '/includes/templates/status.txt';                                                                //EDIT: Path to the Status Template
    var oContainer = $("#container");                                                                                   //EDIT: ID of Parent Container
    var sTemplate = '';                                                                                                 //DO NOT EDIT (Filled by Template)

//FUNCTIONS ====================================================================
    /*  [d]ebug
        echo's out a debug message to the console that is timestamped
        @params     string      required        A message to send to the console
        @returns    nothing
     */
    function debug(Message) {
        console.log("["+(new Date).toLocaleTimeString()+"] "+Message);
    }

    /*  [d]oesBoxExist
        Checks to see if a status box exists in the DOM or not.  This is used
        to determine if we should draw a new box or instead update an existing.
        @params     integer     required        The ID to check for existance
        @returns    boolean                     True/False if the box is found
     */
    function doesBoxExist(ID) {
        //debug("doesBoxExist -> " + ID);
        //debug("> " + $("#"+ID).length > 0);
        return $("#"+ID).length > 0;                                                                                    //If length of data is greater than 0, box exists
    }

    /*  [d]rawNewBox
        Draws a new status box in the DOM.
        @params     integer     required        the unique ID for the box (Usually Database Test.ID)
        @params     string      required        The Test title (e.g.: Google DNS)
        @params     string      required        The Test Address (e.g.: 8.8.8.8:53)
        @params     integer     required        Epoch Timestamp of the last status update (In Seconds)
        @params     integer     required        1/0 for the status (1 = Online, 0 = Offline)
        @returns    nothing                     Draws a status box on screen
     */
    function drawNewBox(ID,Title,Test,Timestamp,Status) {
        //debug("drawNewBox -> " + ID);
        sTemp = sTemplate;
        sTemp = sTemp.replace(/@ID@/g,ID);
        sTemp = sTemp.replace(/@TITLE@/g,Title);
        sTemp = sTemp.replace(/@TEST@/g,Test);
        sTemp = sTemp.replace(/@TIME@/g,Timestamp);
        if(Status == 1) { sStatus = "online"; }
        if(Status == 0) { sStatus = "offline";}
        if(Status == null){ sStatus = "unknown";}
        sTemp = sTemp.replace("@STATUS@", sStatus);
        oContainer.append(sTemp);
    }

    /*  [g]etData
        Queries JSON data from the json page, loops through the results and
        generates/updates the status boxes in the display screen.
        @params     none                        Uses global sURL for the JSON Url
        @returns    nothing                     Requests the Drawing or Updating of status boxes
     */
    function getData() {
        //debug("getData");
        $.getJSON(sURL, function(data) {
            $.each(data, function(index) {
                sTest = data[index].Address;
                if(data[index].Port > 0) { sTest = data[index].Address + ":" + data[index].Port; }
                if(doesBoxExist(data[index].ID) == false) {
                    drawNewBox(data[index].ID, data[index].Title, sTest, timeAgo(data[index].Time), data[index].Status);
                } else {
                    updateBox(data[index].ID, data[index].Title, sTest, timeAgo(data[index].Time), data[index].Status);
                }
            });
        });
    }

    /*  [t]imeAgo
        Takes an epoch timestamp (in seconds) and determines a human readable format.
        For example: "About an hour ago" or "About 43 minutes ago"
        @params     integer     required        The Epoch Time in Seconds
        @returns    string                      A formated string such as "About 3 minutes ago"
     */
    function timeAgo(EpochSeconds) {
        //debug("timeAgo -> " + EpochSeconds);
        if(EpochSeconds == null) { return "No test results found";}
        iDiff = ((new Date).getTime()/1000) - EpochSeconds;
        //debug("> Diff: " + iDiff);
        if(iDiff < 60) { return "A few seconds ago";}                                                                   //Less than 60 seconds ago
        if(iDiff >= 60 && iDiff < 90) { return "About a minute ago";}                                                   //Between 60s & 90s
        if(iDiff >= 90 && iDiff < 3600) { return "About " + Math.floor(iDiff/60) + " minutes ago";}                     //Between 90s and 1 Hour
        if(iDiff >= 3600 && iDiff < 7200) { return "About an hour ago";}                                                //Between 1hr and 1.5 Hours
        if(iDiff >= 7200 && iDiff < 86400) { return "About " + Math.floor(iDiff/3600) + " hours ago";}                  //Between 1.5 Hours and 23 Hours
        return "About " + Math.floor(iDiff/86400) + " days ago";                                                        //A Day or more
    }

    /*  [u]pdateBox
        Updates an existing status box with the most recent information from the JSON data
        @params     integer     required        the ID of the Test Box (usually Test.ID from DB)
        @params     string      required        The Test Title (Usually doesn't change, but just in case)
        @params     string      required        The Test Address (Usually doesn't change, but just in case)
        @params     integer     required        The Epoch (in seconds) timestamp of the last test data
        @params     integer     required        1/0 if the Status is Online (1) or Offline (0)
     */
    function updateBox(ID,Title,Test,Timestamp,Status) {
        //debug("updateBox -> " + ID);
        $("#"+ID+"-status").removeClass("online").removeClass("offline").removeClass("unknown");
        if(Status == 1) { $("#"+ID+"-status").addClass("online")}
        if(Status == 0) { $("#"+ID+"-status").addClass("offline");}
        if(Status == null) { $("#"+ID+"-status").addClass("unknown");}
        $("#"+ID+"-title").html(Title);
        $("#"+ID+"-details").html(Test);
        $("#"+ID+"-time").html(Timestamp);
    }

//TRIGGERS ====================================================================
    $.get(sTemplateURL, function(data){ sTemplate = data});                                                             //Load the status box template
    oContainer.html("");                                                                                                //Clear the page (Defaultly shows "Loading...");
    getData();                                                                                                          //Draws the first Set of Boxes
    setInterval(getData,60000);                                                                                         //Refresh the Boxes every N Milliseconds

});