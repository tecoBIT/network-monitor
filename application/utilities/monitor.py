#!/usr/bin/python

# -----------------------------------------------------------------------------
# LIBRARIES -------------------------------------------------------------------
# -----------------------------------------------------------------------------
import os;                                                                     #Used for Ping
import socket;                                                                 #Used for Ports
import sqlite3 as sqlite;                                                      #Database access
import time;                                                                   #Epoch Timestamps

# -----------------------------------------------------------------------------
# CONFIGURATION ---------------------------------------------------------------
# -----------------------------------------------------------------------------
sDatabase = '/home/test/status.db';                                            #Path to Database (SQLite3)
iMaxTestHistory = 60;                                                          #Maximum amount of history records saved per test

# -----------------------------------------------------------------------------
# GLOBALS ---------------------------------------------------------------------
# -----------------------------------------------------------------------------
oConn = '';                                                                     #SQLite Connection

# -----------------------------------------------------------------------------
# FUNCTIONS -------------------------------------------------------------------
# -----------------------------------------------------------------------------
#[i]sPortOpen
#   Checks if a port is open on a specific ip address
#   @params     string      required        the IP Address to check (e.g.: 8.8.8.8)
#   @params     integer     required        the Port to check (e.g.: 80)
#   @returns    boolean                     true/false if the port is open/available
def isPortOpen(IPAddress,Port):
  sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM);
  result = sock.connect_ex((IPAddress,Port));
  if result == 0:
    return True;
  else:
    return False;

#[i]sServerAlive
#   Pings a server to see if it responds
#   @params     string      required        the IP or Hostname to check
#   @returns    boolean                     true/false if the server responds
def isServerAlive(IPAddress):
  result = os.system("ping -c 1 " + IPAddress + " > /dev/null");
  if result == 0:
    return True;
  else:
    return False;

#[r]ecordResult
#   Records the result of the test into the "Results" table
#   @params     integer     required        The Test ID (From Tests Table)
#   @params     integer     required        1 = Online, 0 = Offline
#   @returns    nothing
def recordResult(ID,Status):
  oCursor2 = oConn.cursor();
  #Insert the Current Data
  sSQL = ("INSERT INTO Results (TestID, Status, Timestamp) "
          "VALUES (" + str(ID) + "," + str(Status) + "," + str(time.time()) + ")")
  oCursor2.execute(sSQL);
  #Purge Outdated records
  sSQL = ("DELETE from Results WHERE TestID = " + str(ID) + " AND ID not in "
          "(SELECT ID FROM Results WHERE TestID = " + str(ID) + " ORDER BY ID DESC LIMIT " + str(iMaxTestHistory) + ")");
  oCursor2.execute(sSQL);
  #Commit and Release
  oConn.commit();
  oCursor2.close();

#[r]emoveOrphans
#   Sorts through the historical data and deletes any records where the
#   parent test has been deleted, and thus orphaned.
#   @params     none
#   @returns    nothing
def removeOrphans():
  oCursor2 = oConn.cursor();
  sSQL = ("DELETE FROM Results WHERE TestID not in (SELECT ID from Tests)");
  oCursor2.execute(sSQL);
  oConn.commit();
  oCursor2.close();

# -----------------------------------------------------------------------------
# LOGIC -----------------------------------------------------------------------
# -----------------------------------------------------------------------------
#Open Database
try:
  oConn = sqlite.connect(sDatabase);
  oCursor = oConn.cursor();
except:
  print "> ERROR! Can not open database";
  e = sys.exc_info()[0];
  print ">> %s" % e;
  sys.exit(1);

#Loop through Tests and record results
oCursor.execute("SELECT ID, Title, Address, Port FROM Tests ORDER BY ID ASC");
for row in oCursor:
  print 'Testing "' + row[1] + '"...';
  if(row[3] == 0):                                                             #Port=0 > Ping Check Else Port Check
    result = isServerAlive(row[2]);
  else:
    result = isPortOpen(row[2],row[3]);
  recordResult(row[0],int(result));

#Clean up any orphaned records
#NOTE:  This can only occur if a human has manually removed a Test.  This
#       merely checks everytime it's run to remove any orphaned database
removeOrphans();

#Done, release controls
oCursor.close();
oConn.close();