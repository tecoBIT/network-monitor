<?php
/*  Modify this file to match your environment then save it as
 *  "global.config.php".   The values below are samples of what
 *  kind of information is expected to be provided to the application.
 */

//Timezone of your Server
    date_default_timezone_set('America/New_York');

//WebServer Domain/IP Configuration (NO TRAILING SLASH)
    define('SITE_HOME',             'http://127.0.0.1');
    define('SITE_COMPRESS',         true);                                                                              //Turns HTML minification on/off before streaming data to client

//Path to the SQLite3 Database
    define('DB_PATH',               '/var/www/application/database/status.db');
?>