<?php
//If No ID, go back to parent
    if($_GET['id'] == ''){ header('location: ' . SITE_HOME . '/?404'); die();}

//Cache Disabling Headers
    header('Cache-Control: no-cache, no-store, must-revalidate');              // HTTP 1.1 Cache Disabling
    header('Pragma: no-cache');                                                // HTTP 1.0  Cache Disabling
    header('Expires: 0');                                                      // Proxies Cache Disabling (Probably unnecessary)
    header('X-Robots-Tag: none');                                              // This tells google bot "noindex,nofollow"

//Minify the HTML Buffer before sending to the client
    function minifyHTML($Buffer) {
        $aSearch = array('/\>[^\S ]+/s',                                       //strip whitespaces after tags, except space
            '/[^\S ]+\</s',                                       //strip whitespaces before tags, except space
            '/(\s)+/s');                                          //shorten multiple whitespace sequences
        $aReplace = array( '>', '<', '\\1' );
        $Buffer = preg_replace($aSearch, $aReplace, $Buffer);
        return $Buffer;
    }
    if(SITE_COMPRESS == true) { ob_start("minifyHTML"); }                      //Toggled in global.config.php

//Convert 1/0/Null to Online/Offline/Unknown
    function getStatus($Status) {
        if($Status == 0) { return "offline";}
        if($Status == 1) { return "online";}
        return "unknown";
    }

//Configuration and Classes
    include_once('application/configuration/global.config.php');
    include_once('application/classes/sqlite.class.php');

//Open up the Database
    $oConn = new SQLiteDatabase(DB_PATH);

//Query Back the Meta Details
//  Doing seperate queries between Meta & Results in case there are no results yet
    $sSQL = 'SELECT Title, Address, Port FROM Tests WHERE ID = :ID';
    $aParams = array(':ID' => $_GET['id']);
    $aMeta = $oConn->query($sSQL,$aParams);

//Query Back the Test Details
    $sSQL = 'SELECT Timestamp, Status FROM Results WHERE TestID = :ID ORDER BY ID DESC';
    $aResults = $oConn->query($sSQL,$aParams);


?>
<html>
    <head>
        <title><?php echo $aMeta[0]['Title'] ?> Test Results | Network Monitor</title>
        <meta name="robots" content="noindex,nofollow" />
        <meta name="viewport" content="width=device-width, initial-scale=0.75" />
        <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
        <meta content="utf-8" http-equiv="encoding">
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="expires" content="Sun, 15 Aug 1976 10:34:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
        <link rel="canonical" href="<?php echo SITE_HOME ?>/details.php" />
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
        <link href="includes/css/bootstrap.min.css" rel="stylesheet" />
        <link href="includes/css/style.min.css" rel="stylesheet" />
        <script src="includes/js/jquery.min.js" defer></script>
        <script src="includes/js/global.min.js" defer></script>
    </head>
    <body>
        <div id="container-sm">
            <div class="col-md-12 detailsbox box">
                <h1><?php echo $aMeta[0]['Title']; ?></h1>
                <h2><?php echo $aMeta[0]['Address']; if($aMeta[0]['Port'] > 0){ echo ':' . $aMeta[0]['port']; }?></h2>
                <hr />
            <?php
            if(count($aResults) > 0) {
                foreach ($aResults AS $Result) { ?>
                    <div class="row">
                        <div class="col-md-4"><?php echo date('F jS Y', $Result['Timestamp']) ?></div>
                        <div class="col-md-6"><?php echo date('h:i:s A', $Result['Timestamp']) ?></div>
                        <div class="col-md-2"><span
                                class="<?php echo getStatus($Result['Status']) ?>"><?php echo getStatus($Result['Status']) ?></span>
                        </div>
                    </div><?php
                }
            } else { ?>
                <div class="row">
                    <div class="col-md-12">No test results found</div>
                </div><?php
            }?>
            </div>
        </div>
    </body>
</html>