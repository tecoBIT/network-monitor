# Network Monitor
The purpose of the Network-Monitor project is to provide a simplistic interface to briefly monitor specific network 
services and machines by Port testing or Pinging.   This should not be considered a robost solution for enterprise 
networks but instead a grass roots solution for home networking.


## Requirements
* PHP 5.5+
* SQLite 3
* Python 2.7 or Above
* Webserver of sorts (Apache,nginx,etc)

### Optional Requirements
* libnss-winbind (hostname resolution)


##Configuration
1. Open "sample-global.config.php" from "/application/configuration" in your favorite text editor
2. On line 8 set the default timezone to match your [preferred location](http://php.net/manual/en/timezones.php)
3. On line 11, set the fully qualified domain name for your site (e.g.: http://mysite.local)
4. On line 12, optionally toggle the HTML Minification on/off (If on, less data, more CPU.  If off, less CPU more data used)
5. On line 15, adjust the path to match your database's location (e.g.: /var/www/mysite/application/database/status.db)
6. Save these changes "global.config.php"
7. Open "monitor.py" in from "/application/utilities" in your favorite text editor
8. On line 14, adjust the value for "sDatabase" to be the path to your database (e.g.: /var/www/mysite/application/database/status.db)
9. On line 15, optionally adjust the number of historical records to keep per test (default: 60)
10. Use an SQLite Browser to open the status.db "Tests" table (Example: [SQLiteBrowser](http://sqlitebrowser.org/))
11. Add as many "Tests" as you would like the system to monitor.   If the port is set to 0, the script will ping the address, but if a port is specified, we will attempt to connect directly to the port.


##Setup
The following setup instructions assume you have already setup and patched your own linux server on your favorite distribution.  Additionally it is assume 
that a web server has been setup and configured as well.  This project was developed utilizing Apache2 on Debian8 but should suffice on any major distribution.

1. Adjust permissions for the monitor.py script to be allowed to execute (sudo chmod +x /application/utilities/monitor.py)
2. Create a CronJob that will periodically run the monitor script to check your Test availability.  The example below will run the monitor script every 5 minutes.
  * sudo crontab -e
  * */5 * * * * /usr/bin/python /var/www/mysite/application/utilities/monitor.py
3. Set the proper permission for the database to be allowed to write
  * sudo chmod 2755 /application/database/status.db
  
###Optional Setup
If you plan to monitor some internal/local systems by their hostname instead of an IP Address or Domain name you will need to setup winbind

1. sudo apt-get update
2. sudo apt-get install libnss-winbind
3. sudo nano /etc/nsswitch.conf
  * Change the hosts line to "hosts: files wins mdns4_minimal dns myhostname"
  * save & close
4. sudo /etc/init.d/networking restart
 
You should now be able to ping computers by name (e.g.: ping JimsLaptop)


##Notes
An .htaccess file has been provided that is pre-configured for an Apache environment.  The settings are as follows:

+ Disable folder contents display
+ Protects the application folder from non server-side access
+ Enables caching of CSS/JS for up to a week
+ Disables caching of HTML contents
+ Enabled DEFLATE compression for outbound contents

If you are utilizing Apache, the htaccess assumes the following mods have been installed. Adjust if needed

+ mod_deflate
+ mod_header
+ mod_rewrite

#License
Copyright 2016. Released under [GPLv3 license](http://www.gnu.org/licenses/gpl-3.0.en.html).

> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation, either version 3 of the License
>  
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU General Public License for more details.
> 
> You should have received a copy of the GNU General Public License
> along with this program.  If not, see http://www.gnu.org/licenses/